# Homework 2 - Testing

## Background

Currently some simple tests for the Name o' Matic can be found at [`main.test.js`](./tests/main.test.js).
Let's try to improve the strength of these tests!

## Prerequisites

**Install NodeJS**

You will need to have NodeJS installed ([see instructions](https://nodejs.org/en/)). Any version around 12 should be fine.

You can verify this is installed by running:

```bash
# Verify node is installed
node --version

# Verify that npm came with it
npm --version
```

**Install local dependencies**

Once `node` and `npm` are installed. Clone this repo (or a fork of this repo) and `cd` into the directory.

Now you can download the local dependencies needed to run [Jest](https://jestjs.io/), the testing framework, for
this project.

```bash
npm install
```

**Run the tests**

Now, you should be able to run the tests locally with:

```
npm test
```

The tests should pass with no errors.

## Instructions

Update [`main.test.js`](./tests/main.test.js) such that the test suite will **fail** if the following bug causing changes were introduced
into the code.

In other words, the tests **should** fail if any of these changes were introduced 👇:

### 1. Change causing `pickRandom` bug

```patch
diff --git a/public/main.js b/public/main.js
index 98f88f4..7348b2e 100644
--- a/public/main.js
+++ b/public/main.js
@@ -4,7 +4,7 @@ const LOADER_HTML = `<svg class="animate-spin ml-1 h-5 w-5 inline-block text-bla
         </svg>`;

 const pickRandom = (names) => {
-  return names[Math.floor(Math.random() * names.length)];
+  return names[3];
 };

 const getNamesFromTextarea = (textarea) => {

```

<small>You can apply these patches by copying it to your clipboard and running something like `pbpaste | git apply` (on mac)</small>

### 2. Change causing `showLoader` bug

```patch
diff --git a/public/main.js b/public/main.js
index 98f88f4..b64ef54 100644
--- a/public/main.js
+++ b/public/main.js
@@ -15,7 +15,7 @@ const getNamesFromTextarea = (textarea) => {
 };

 const showLoader = (el) => {
-  el.innerHTML = LOADER_HTML;
+  el.innerHTML += LOADER_HTML;
 };

 const showName = (el, name) => {

```

### 3. Change causing `showName` bug

```patch
diff --git a/public/main.js b/public/main.js
index 98f88f4..50a0b32 100644
--- a/public/main.js
+++ b/public/main.js
@@ -19,7 +19,7 @@ const showLoader = (el) => {
 };

 const showName = (el, name) => {
-  el.textContent = name;
+  el.textContent += name;
 };

 const main = () => {

```

### (BONUS) 4. Change removing `isLoading` check

```patch
diff --git a/public/main.js b/public/main.js
index 98f88f4..a3cf286 100644
--- a/public/main.js
+++ b/public/main.js
@@ -27,22 +27,13 @@ const main = () => {
   const elName = document.getElementById("js-name");
   const elTextarea = document.getElementById("js-textarea");

-  let isLoading = false;
-
   const onButtonClick = () => {
-    if (isLoading) {
-      return;
-    }
-
-    isLoading = true;
     showLoader(elName);

     setTimeout(() => {
       const names = getNamesFromTextarea(elTextarea);

       showName(elName, pickRandom(names));
-
-      isLoading = false;
     }, 400);
   };

```

## Hint

It will probably be helpful to review Jest's API:

- https://jestjs.io/docs/next/expect
- https://jestjs.io/docs/next/mock-function-api#mockfnmockreturnvaluevalue
