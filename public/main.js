const LOADER_HTML = `<svg class="animate-spin ml-1 h-5 w-5 inline-block text-black" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24">
          <circle class="opacity-25" cx="12" cy="12" r="10" stroke="currentColor" stroke-width="4"></circle>
          <path class="opacity-75" fill="currentColor" d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"></path>
        </svg>`;

const pickRandom = (names, index) => {
  return names[index];
};

const getNamesFromTextarea = (textarea) => {
  return textarea.value
    .split("\n")
    .map((x) => x.trim())
    .filter(Boolean);
};

const showLoader = (el) => {
  el.innerHTML = LOADER_HTML;
};

const showName = (el, name) => {
  el.textContent = name;
};

function shuffle(a) {
  var j, x, i;
  for (i = a.length - 1; i > 0; i--) {
    j = Math.floor(Math.random() * (i + 1));
    x = a[i];
    a[i] = a[j];
    a[j] = x;
  }
  return a;
}


const main = () => {
  const elButton = document.getElementById("js-button");
  const elName = document.getElementById("js-name");
  const elTextarea = document.getElementById("js-textarea");
  
  const names = getNamesFromTextarea(elTextarea);
  const namesShuffled = shuffle(names);
  const count = -1;

  let isLoading = false;

  const onButtonClick = () => {
    if (isLoading) {
      return;
    }

    isLoading = true;
    showLoader(elName);

    setTimeout(() => {
      const namesCopy = getNamesFromTextarea(elTextarea)
            
      if(!([...namesCopy].sort().toString() === [...names].sort().toString())){
        names = [... namesCopy];
        namesShuffled = shuffle(names);
        count = -1;
      }

      count >= names.length - 1 ? count = 0 : count++;

      showName(elName, pickRandom(namesShuffled, count));

      isLoading = false;

    }, 400);
  };



  elButton.addEventListener("click", onButtonClick);
};

main();